<?php 
function($date, $type) {
	//get cache
    $data = Yii::$app->cache->get('data');
    if ($data) return $data;

    $userId = Yii::$app->user->id;
    $dataList = SomeDataModel::find()->where(['date' => $date, 'type' => $type, 'user_id' => $userId])->all();
    $result = [];

    if (!empty($dataList)) {
        foreach ($dataList as $dataItem) {
            $result[$dataItem->id] = ['a' => $dataItem->a, 'b' => $dataItem->b];
        }
    }

	//set cache
    Yii::$app->cache->set('data',$result), 60);
    
    return $result;
}
?>