<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Регистрация пользователя';
$this->params['breadcrumbs'][] = $this->title;
?>
    <div class="site-signup ">
        <h1><?= Html::encode($this->title) ?></h1>
        <p>Заполните поля для регистрации пользователя:</p>
        <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>
            <fieldset>
                <?= $form->field($model, 'fullname')->textInput(['autofocus' => true, 'placeholder'=>'Введите ФИО']) ?>
                <?= $form->field($model, 'email')->textInput(['placeholder'=>'Введите почту']) ?>
                <?= $form->field($model, 'password')->passwordInput(['placeholder'=>'Придумайте пароль']) ?>
                <?= $form->field($model, 'type')->dropDownList(
                    [
                        0 => 'Физ. лицо',
                        1 => 'Юр.лицо'
                    ]) ?>
                <? $model->individual_entrepreneur = '0'; ?>
                <?= $form->field($model, 'individual_entrepreneur')->radioList(
                    [
                        1 => 'Да',
                        0 => 'Нет'
                    ],
                    [
                        'item' => function($index, $label, $name, $checked, $value) {
                            $checked = ($checked) ? 'checked' : '';
                            $return = '<label class="radio-inline">';
                            $return .= '<input type="radio" class="individual_entrepreneur" name="' . $name . '" value="' . $value . '" ">';
                            $return .= '<span>' . ucwords($label) . '</span>';
                            $return .= '</label>';
                            return $return;
                        }
                    ]
                    ) ?>
                <?= $form->field($model, 'inn')->textInput(['placeholder'=>'Введите ИНН']) ?>
                <?= $form->field($model, 'name_organ')->textInput(['placeholder'=>'Введите название организации'])?>
                <div class="form-group">
                    <?= Html::submitButton('Зарегистрироваться', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
                </div>
            </fieldset>
        <?php ActiveForm::end(); ?>
    </div>

<?php
$js = <<<JS
    $('.field-signupform-name_organ').addClass('hide');
    $('.field-signupform-inn').addClass('hide');
    $('.individual_entrepreneur').on('change',function() {
        if ($(this).val() == 1)
            $('.field-signupform-inn').removeClass('hide');
        else
            $('.field-signupform-inn').addClass('hide');
    });
    $('#signupform-type').on('change',function() {
        if ($(this).val() == 1)
        {
            $('.field-signupform-individual_entrepreneur').addClass('hide');
            $('.field-signupform-name_organ').removeClass('hide');
            $('.field-signupform-inn').removeClass('hide');
        }
        else
        {
            $('.field-signupform-individual_entrepreneur').removeClass('hide');
            $('.field-signupform-name_organ').addClass('hide');
            $('.field-signupform-inn').removeClass('hide');
        }    
    });
JS;

$this->registerJs($js);

?>

