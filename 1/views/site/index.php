<?php

/* @var $this yii\web\View */
use yii\helpers\Url;
$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <?php if (Yii::$app->user->isGuest):?>
            <h1>Добрый день!</h1>
            <p class="lead">Вы можете зарегистрироваться на сайте.</p>
            <p><a class="btn btn-lg btn-success" href="<?= Url::to(['site/signup']) ?>">Регистрация</a></p>
        <?php else:?>
            <h1>Добрый день, <?= Yii::$app->user->identity->fullname ?>!</h1>
            <?php if (Yii::$app->user->identity->type == 0):?>
                <p class="lead">Тип: Физическое лицо</p>
                <?php if (Yii::$app->user->identity->individual_entrepreneur == 1):?>
                    <p class="lead">Индивидуальный предприниматель ( ИНН: <?= Yii::$app->user->identity->inn ?>) </p>
                <?php endif;?>

            <?php else:?>
                <p class="lead">Тип: Юридическое лицо</p>
                <p class="lead">Наименование организации: <?= Yii::$app->user->identity->name_organ  ?></p>
                <p class="lead">ИНН: <?= Yii::$app->user->identity->inn  ?></p>


            <?php endif;?>
            <p class="lead">Зарегистрирован: <?= Yii::$app->formatter->asDatetime(Yii::$app->user->identity->created_at, "php:d.m.Y H:i:s"); ?></p>
        <?php endif;?>




    </div>


</div>
