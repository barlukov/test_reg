<?php
/**
 * Created by PhpStorm.
 * User: biletavto
 * Date: 18.07.2017
 * Time: 9:36
 */

namespace app\models;
use Yii;
use yii\base\Model;

class SignupForm extends Model
{
    public $email;
    public $password;
    public $fullname;
    public $type;
    public $individual_entrepreneur;
    public $inn;
    public $name_organ;

    public function rules()
    {
        return [
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\app\models\User', 'message' => 'This email address has already been taken.'],
            ['password', 'required'],
            ['password', 'string', 'min' => 6],

            [['fullname', 'type', 'individual_entrepreneur'], 'required'],

            ['inn', 'required', 'when' => function ($model) {
                return $model->type == '1';
            }, 'whenClient' => "function (attribute, value) {
            return $('#signupform-type').val() == '1';
            }"],

            ['inn', 'required', 'when' => function ($model) {
                return $model->individual_entrepreneur == '1';
            }, 'whenClient' => "function (attribute, value) {
            return $('.individual_entrepreneur:checked').val() == '1';
            }"],

            ['name_organ', 'required', 'when' => function ($model) {
                return $model->type == '1';
            }, 'whenClient' => "function (attribute, value) {
            return $('#signupform-type').val() == '1';
            }"],

            ['inn', 'trim'],
        ];
    }

    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }

        $user = new User();
        $user->email = $this->email;
        $user->setPassword($this->password);
        $user->generateAuthKey();

        $user->fullname = $this->fullname;
        $user->type = $this->type;
        $user->individual_entrepreneur = $this->individual_entrepreneur;
        $user->inn = $this->inn;
        $user->name_organ = $this->name_organ;


        return $user->save() ? $user : null;
    }

    public function attributeLabels()
    {
        return [
            'fullname' => 'ФИО',
            'type' => 'Тип',
            'email'=> 'E-mail',
            'password'=>'Пароль',
            'individual_entrepreneur'=>'Индивидульный предприниматель',
            'inn'=>'ИНН',
            'name_organ'=>'Название организации',
        ];
    }



}