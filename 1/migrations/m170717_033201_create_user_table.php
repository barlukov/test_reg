<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 */
class m170717_033201_create_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('user', [
            'id' => $this->primaryKey(),
            'email' => $this->string()->notNull()->unique(),
            'auth_key' => $this->string(32)->notNull(),
            'password_hash' => $this->string()->notNull(),
            'password_reset_token' => $this->string()->unique(),
            'status' => $this->smallInteger()->notNull()->defaultValue(10),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'fullname' => $this->string()->notNull(),
            'type' => $this->boolean()->notNull(),
            'individual_entrepreneur' => $this->boolean()->notNull(),
            'inn' => $this->integer()->null(),
            'name_organ' => $this->string()->null(),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('user');
    }
}
